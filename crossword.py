from PIL import Image
import numpy
import itertools
import urllib.request
import io
import json

IMAGE_REQUEST_URL = 'http://api-fotki.yandex.ru/api/podhistory/poddate/?limit=1&format=json'


# image_array must be grayscale
def __generate_crossword(image_array):
    crossword = dict()

    crossword['rows'] = __get_crossword_side(image_array)
    crossword['columns'] = __get_crossword_side(image_array.T)

    return crossword


def __get_crossword_side(image_array):
    side = []

    for row_index, row in enumerate(image_array):
        side.append([])

        for is_black, pixels in itertools.groupby(row, lambda color: color < 128):
            if is_black:
                side[row_index].append(sum(1 for _ in pixels))

    return side


def __get_daily_photo_url():
    with urllib.request.urlopen(IMAGE_REQUEST_URL) as url:
        response = json.loads(url.read().decode(url.info().get_param('charset') or 'utf-8'))

    return response['entries'][0]['img']['XS']['href']


def __get_image(image_url):
    with urllib.request.urlopen(image_url) as url:
        image_file = io.BytesIO(url.read())

    return Image.open(image_file)


def get_crossword():
    image_url = __get_daily_photo_url()
    image = __get_image(image_url)
    image_array = numpy.array(image.convert('L'))

    return __generate_crossword(image_array)
