import requests


def get_best_post():
    url = 'https://api.producthunt.com/v1/posts'
    params = {'day': '2015-12-02'}
    headers = {'Authorization': 'Bearer 6d552893f3b9854990e72be247c466714e1e00251ee4cedb87a743d46ce28a91'}

    response = requests.get(url, params=params, headers=headers)
    posts = response.json()['posts']
    sorted_posts = sorted(posts, key=lambda k: k['votes_count'], reverse=True)

    best_post = sorted_posts[0]
    filtered_best_post = {
        'name': best_post['name'],
        'image_url': best_post['thumbnail']['image_url'],
        'url': best_post['redirect_url']
    }

    return filtered_best_post
