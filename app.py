import json
import posts
import crossword


def app():
    news = {}

    try:
        news['product_of_the_day'] = {
            'title': 'Tech product of the day',
            'data': posts.get_best_post()
        }
    except:
        print('Failed to get product of the day')

    try:
        news['japanese_crossword'] = {
            'title': 'Japanese crossword',
            'data': crossword.get_crossword()
        }
    except:
        print('Failed to generate japanese crossword')

    return json.dumps(news)


result = app()
